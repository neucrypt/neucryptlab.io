---
date: 2019-08-01
linktitle: Sampling
title: Sampling
weight: 10
paper: https://eprint.iacr.org/2019/823
gitlink: https://gitlab.com/neucrypt/securely_sampling
---

This project implements the batched sampling method as a secure computation for implementing differentially private mechanisms such as report-noisy-max.
