---
date: 2015-12-01
linktitle: Anonize2
title: Anonize2
weight: 10
paper: https://anonize.org
gitlink: https://gitlab.com/neucrypt/anonize2
---

This project implements the updated Anonize2 library for running cryptographically authenticated and anonymous surveys.  See the anonize.org project page for links to papers.
