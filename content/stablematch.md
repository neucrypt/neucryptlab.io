---
date: 2016-03-01
linktitle: Secure stable matching
title: Secure Stable Matching
weight: 10
paper: https://shelat.ccis.neu.edu/research/2016-08-01-stablematching/
gitlink: https://gitlab.com/neucrypt/secure-stable-matching
---

This project implements the secure stable matching algorithm presented in [DES16].