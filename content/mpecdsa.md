---
date: 2018-05-01
linktitle: mpecdsa
title: mpecdsa
weight: 10
paper: https://eprint.iacr.org/2018/499
gitlink: https://gitlab.com/neucrypt/mpecdsa
---

This project implements the 2-out-of-n threshold ECDSA protocol for setup and signing described in our Oakland'2018 paper, ``Secure Multi-party Threshold ECDSA from ECDSA Assumptions."
